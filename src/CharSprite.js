/**
 * Created by nesto_000 on 4/04/15.
 */
var CharSprite = cc.Sprite.extend({
    _currentRotation:0,
    _scale:0.25,
    _status:null,
    _label:null,
    ctor:function(x,y,char,anc){
        var ancX = anc;

        this._super();
        this.x = x-ancX/2;
        this.y = y-anc/2;
        this.rotation=0;
        this.name= "char_"+char;
        this._char = char;

        var draw = new cc.DrawNode();
        var draw2 = new cc.DrawNode();
        this.addChild(draw, 10);
        this.addChild(draw2, 10);


        draw.drawRect(cc.p(0,0), cc.p(anc, anc), cc.color(255, 255, 255, 0), 2, cc.color(255, 255, 255, 255));
        draw2.drawRect(cc.p(4,4), cc.p(anc-4, anc-4), cc.color(255, 255, 255, 50), 2, cc.color(255, 255, 255, 0));

        this._label = new cc.LabelTTF(char, "Arial", 38);
        // position the label on the center of the screen
        this._label.x = anc/2;
        this._label.y = anc/2;
        this._label.visible=false;
        this._label.color = cc.color(0,0,0);
        this.addChild(this._label, 11);
    },
    effectClickGood: function(){
        cc.log("efectClickGood --  "+this._n);

        var sprite_action1 = cc.ScaleTo.create(0.2,-this._scale-0.2,this._scale+0.2);
        var sprite_action2 = cc.ScaleTo.create(0.2,this._scale+0.4,this._scale+0.4);
        var sprite_action21 = cc.DelayTime.create(0.2);
        var sprite_action3 = cc.ScaleTo.create(0.2,-this._scale,this._scale);
        var sprite_action4 = cc.ScaleTo.create(0.2,0,0);
        this.zIndex = 5;
        this.runAction(cc.sequence(sprite_action1,sprite_action2,sprite_action21,sprite_action3,sprite_action4));
    },
    effectClickBad: function(){
        cc.log("efectClickGood --  "+this._n);

        var sa1 = cc.ScaleTo.create(0.1,this._scale+0.05,this._scale+0.05);
        var sa2 = cc.ScaleTo.create(0.1,this._scale-0.05,this._scale-0.05);
        var sa3 = cc.ScaleTo.create(0.1,this._scale,this._scale);
        var sa4 = cc.RotateTo.create(0.1,36);
        var sa5 = cc.RotateTo.create(0.1,-36);
        var sa6 = cc.RotateTo.create(0.1,0);
        var soundWrong = cc.callFunc(function (t) {
            cc.audioEngine.playEffect(res.Wrong_sound);
        }.bind(this));
        var destroyMe = cc.callFunc(function (t) {
            var spriteRed = new cc.Sprite("res/Sprite/letra_roja_-"+ t._n+".png");
            spriteRed.x = t.x;
            spriteRed.y = t.y;
            spriteRed.scale = t.scale;
            var that = t.getParent();
            that.removeChild(t);
            that.addChild(spriteRed);
        }.bind(this));
        this.zIndex = 5;
        // this.runAction(cc.TintBy.create(1,0,-510,-510));
        this.runAction(cc.sequence(soundWrong,sa1,sa2,sa1,sa2,sa1,sa2,sa1,sa3,destroyMe));
        this.runAction(cc.sequence(sa4,sa5,sa4,sa5,sa4,sa5,sa4,sa6));
    },
    getStatus:function () {
        return this._status;
    }
});