/**
 * Created by nesto_000 on 4/04/15.
 */
/**
 * Created by nesto_000 on 3/04/15.
 */
var ClockSprite = cc.Sprite.extend({
    _currentRotation:0,
    _scale:0.25,
    _time:15,
    _status:null,
    _label:null,
    _left:null,
    _sc:null,
    _to1:null,
    _count:null,
    _repeat1:null,
    _repeat2:null,
    ctor:function(x,y){
        this._super();
        this.x = x;
        this.y = y;
        this._count=this._time;
        var soundClock = cc.callFunc(function (t) {
            if (t._count==0){
                cc.audioEngine.playEffect(res.Clock_end_sound);
            }else{
                cc.audioEngine.playEffect(res.Clock_sound);
            }
        }.bind(this));
        var chageNum = cc.callFunc(function (t) {
            t._count-=1;
            if (t._count==0){
                t.visible=false;
            }
            t._label.string = t._count;
        }.bind(this));
        var dl = cc.DelayTime.create(1);
        this._sc = cc.Sequence.create(soundClock,dl,chageNum);

        this._to1 = cc.progressFromTo(1, 0, 100);
        var sprite = new cc.Sprite(res.Prestaya);
        this._left = new cc.ProgressTimer(sprite);
        this._left.type = cc.ProgressTimer.TYPE_RADIAL;
        this._left.scale=0.40;
        this._left.opacity=100;
        this.addChild(this._left);

        this._label = new cc.LabelTTF(this._count, "Arial", 140);
        // position the label on the center of the screen
        this._label.color = cc.color(0,60,0);
        this.addChild(this._label, 11);
        this.visible=false;

        this._repeat1 = cc.Repeat.create(this._to1,this._time);
        this._repeat2 = cc.Repeat.create(this._sc,this._time);
    },
    initClock: function(){
        this.stopClock();
        this._count=this._time;
        this.visible=true;
        this._left.runAction(this._repeat1);
        this.runAction(this._repeat2);
    },
    stopClock: function(){
        this._count=this._time;
        this._label.string = this._count;
        this.stopAllActions();
        this._left.stopAllActions();
        cc.audioEngine.stopAllEffects();
        this._sc.stop();
        this._to1.stop();
        this._repeat1.stop();
        this._repeat2.stop();
    }
});
