/**
 * Created by nesto_000 on 21/03/15.
 */
var casilla;
var casillas;
var cont_win;
var chars;
var can_play=false;
var totalChart=null;
var countChart=null;
var countChartWrong=null;
var countclicks=null;
var PlayLayer = cc.Layer.extend({
    sprite:null,
    _con_win:null,
    _last_sprite:null,
    _clock:null,
    _emitter1:null,
    _emitter2:null,
    _emitter3:null,
    ctor:function () {
        //////////////////////////////
        // 1. super init first
        this._super();
        var size = cc.winSize;
        countclicks=0;
        // Imgenes, botones basicos de juego

        var imageSprite = new cc.Sprite.create(res.Backgound_play);
        imageSprite.attr({
            x: size.width / 2,
            y: size.height / 2
        });
        var imageSize = imageSprite._contentSize;
        imageSprite.scaleX = size.width / imageSize.width;
        imageSprite.scaleY = size.height / imageSize.height;
        this.addChild(imageSprite, 0);

        var Logo1 = new cc.Sprite(res.Banco_popular);
        Logo1.attr({
            x: size.width / 2 + 100,
            y: size.height / 2 + 200,
            scale:0.60
        });
        this.addChild(Logo1, 1);

        var Logo2 = new cc.Sprite.create(res.Prestaya);
        Logo2.attr({
            x: size.width / 2 + 400,
            y: size.height / 2 + 200,
            scale:0.25
        });
        this.addChild(Logo2, 1);


        cont_win=0;
        if (can_play){
            countChartWrong=0;
            countChart=0;
            // reconocer el evento del mouse
            var listener1 = cc.EventListener.create({
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                swallowTouches: true,
                onTouchBegan: function (touch, event) {

                    var target = event.getCurrentTarget();

                    var locationInNode = target.convertToNodeSpace(touch.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);

                    if (cc.rectContainsPoint(rect, locationInNode)) {
                        if (can_play){
                            if(target._clickable){
                                var exist = false;
                                for (var i in chars) {
                                    if(chars[i]._char == target._key){
                                        //target.effectClickGood();
                                        chars[i]._label.visible=true;
                                        exist=true;
                                        countChart+=1;
                                    }
                                }

                                if (exist){
                                    target.getParent()._clock.stopClock();
                                    target.effectClickGood();
                                }else{
                                    target.getParent()._clock.stopClock();
                                    target.effectClickBad();
                                    countChartWrong+=1;
                                    target.getParent().createDead(countChartWrong);
                                }
                                if(countChartWrong<7){
                                    countclicks+=1;
                                    if(countclicks>=3){
                                        target.getParent()._clock.stopClock();
                                        target.getParent()._clock.initClock();
                                    }
                                    if(countChart==totalChart){
                                        target.getParent()._clock.stopClock();
                                        can_play=false;
                                        cc.audioEngine.playEffect(res.Win_sound);
                                        target.getParent().createmenuNewGame();
                                        target.getParent().initParticules();
                                    }
                                }else if(countChartWrong==7){
                                    target.getParent()._clock.stopClock();
                                    can_play=false;
                                    cc.audioEngine.playEffect(res.Lose_sound);
                                    target.getParent().createmenuNewGame();
                                }
                            }
                        }
                        return true;
                    }
                    return false;
                }
            });

            var listener2 = cc.EventListener.create({
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                swallowTouches: true,
                onTouchBegan: function (touch, event) {

                    var target = event.getCurrentTarget();

                    var locationInNode = target.convertToNodeSpace(touch.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);

                    if (cc.rectContainsPoint(rect, locationInNode)) {
                        if (can_play){
                            for (var i in chars) {
                                chars[i]._label.visible=true;
                            }
                            target.getParent()._clock.stopClock();
                            can_play=false;
                            cc.audioEngine.playEffect(res.Win_sound);
                            target.getParent().createmenuNewGame();
                            target.getParent().initParticules();
                        }
                        return true;
                    }
                    return false;
                }
            });

            var n="";
            var anc = 45;
            var esp = 10;
            var posx= 0;
            var posy=0;
            var abc="abcdefghijklmnñopqrstuvwxyz",
            abc=abc.toUpperCase();

            for (var i=0;i<27;i++){
                if(i==0){
                    posx= (size.width / 2)-(8*anc+7*esp)/2+anc/2;
                    posy=size.height / 4-10;
                }else if(i==8){
                    posx= (size.width / 2)-(10*anc+9*esp)/2+anc/2;
                    posy-=anc+esp;
                }else if(i==18){
                    posx= (size.width / 2)-(9*anc+8*esp)/2+anc/2;
                    posy-=anc+esp;
                }
                if (i<9){
                    n="0"+(i+1);
                }else{
                    n=(i+1);
                }
                var tecla = new TeclaSprite(posx,posy,n,abc.slice(i,i+1));
                this.addChild(tecla, 2);
                if (i==0){
                    cc.eventManager.addListener(listener1, tecla);
                }else{
                    cc.eventManager.addListener(listener1.clone(), tecla);
                }
                posx+=anc+esp;
            }

            var ancChar = 40;


            numr=Math.floor((Math.random()*Phrases.length)+0);

            var palabras=Phrases[numr].toUpperCase().split(" ");
            var left=0;
            var sep=5;
            var cont=0;
            var cut = [];
            var sizeLine = [];
            chars = [];
            sizeLine[cont]=0;

            for (var i = 0; i < palabras.length; i++) {
                var incre = ancChar*palabras[i].length+sep*(palabras[i].length-1);
                if ((left+incre)>size.width/2+100) {
                    cut[cont]=i-1;
                    sizeLine[cont]= left-ancChar/2;
                    left=0;
                    cont+=1;
                }
                left+=incre+ancChar/2;
            }
            sizeLine[cont] = left-ancChar/2;
            var espY = size.height/2+5*ancChar/4*(cut.length)/2;
            cont=0;
            left=0;
            totalChart=0;
            for (var i = 0; i < palabras.length; i++) {
                if(left==0){
                    left+=size.width/2-sizeLine[cont]/2;
                }
                var pala = palabras[i];

                for (var j = 0; j < pala.length; j++) {
                    var char1 = new CharSprite(left,espY,pala[j],ancChar);
                    this.addChild(char1, 2);
                    if (abc.indexOf(pala[j])>-1){
                        chars.push(char1);
                        totalChart+=1;
                    }else{
                        char1._label.visible=true;
                    }
                    left+=(ancChar+sep);
                }
                if (cut[cont]==i){
                    espY-=5*ancChar/4;
                    left=0;
                    cont+=1;
                }else{
                    left+=ancChar/2;
                }
            }
            var spriteStar = new cc.Sprite(res.Star);
            spriteStar.x = 180;
            spriteStar.y = 150;
            spriteStar.scale = 0.5;
            this.addChild(spriteStar);
            cc.eventManager.addListener(listener2, spriteStar);

            var sp1 = cc.TintTo.create(1,255,255,255);
            var sp2 = cc.TintTo.create(1,220,220,202);
            var sps = cc.Sequence.create(sp1,sp2);
            spriteStar.runAction(cc.repeatForever(sps));

            var labelNum = new cc.LabelTTF((numr+1), "Arial", 44);
            // position the label on the center of the screen
            labelNum.x = 180;
            labelNum.y = 150;
            labelNum.color = cc.color(255,100,0);
            this.addChild(labelNum);
        }else{
            this.createmenuNewGame();
        }

        this._clock = new ClockSprite();
        this._clock.x = size.width/6;
        this._clock.y = 4*size.height/5-50;
        this._clock.scale = 0.5;
        this.addChild(this._clock);

        return true;
    },
    createDead:function(i){
        if(i<=7){
            var size = cc.winSize;
            var posx = 3*size.width / 4+150;
            var posy = size.height / 2-280;
            if(i==1){
                var spriteWrong = new cc.Sprite(res["Wrong_1"]);
                spriteWrong.x = posx;
                spriteWrong.y = posy;
                spriteWrong.scale = 0.2;
            }else if(i==2){
                var spriteWrong = new cc.Sprite(res["Wrong_2"]);
                spriteWrong.x = posx-58;
                spriteWrong.y = posy-22;
                spriteWrong.scale = 0.25;
            }else if(i==3){
                var spriteWrong = new cc.Sprite(res["Wrong_3"]);
                spriteWrong.x = posx-60;
                spriteWrong.y = posy+110;
                spriteWrong.scale = 0.18;
            }else if(i==4){
                var spriteWrong = new cc.Sprite(res["Wrong_4"]);
                spriteWrong.x = posx-48;
                spriteWrong.y = posy+160;
                spriteWrong.scale = 0.25;
            }else if(i==5){
                var spriteWrong = new cc.Sprite(res["Wrong_5"]);
                spriteWrong.x = posx-24;
                spriteWrong.y = posy+180;
                spriteWrong.scale = 0.22;
            }else if(i==6){
                var spriteWrong = new cc.Sprite(res["Wrong_6"]);
                spriteWrong.x = posx-7;
                spriteWrong.y = posy+180;
                spriteWrong.anchorY = 1;
                spriteWrong.scale = 0.20;
                var sp1 = cc.RotateTo.create(1,10);
                var sp2 = cc.RotateTo.create(1,-10);
                var sps = cc.Sequence.create(sp1,sp2);
                spriteWrong.runAction(cc.repeatForever(sps));
                this.addChild(spriteWrong);

                var spriteWrong = new cc.Sprite(res["Wrong_7"]);
                spriteWrong.x = posx-4;
                spriteWrong.y = posy+180;
                spriteWrong.scale = 0.20;
                spriteWrong.anchorY = 1.3;
                var sp1 = cc.RotateTo.create(1,10);
                var sp2 = cc.RotateTo.create(1,-10);
                var sps = cc.Sequence.create(sp1,sp2);
                spriteWrong.runAction(cc.repeatForever(sps));
                spriteWrong.visible=false;
                this._last_sprite = spriteWrong;
            }else if(i==7){
                this._last_sprite.visible=true;
            }
            if(i<7){
                this.addChild(spriteWrong);
            }
        }
    },
    createmenuNewGame: function(){
        var size = cc.winSize;
        var draw = new cc.DrawNode();
        this.addChild(draw, 10);
        draw.drawRect(cc.p(0, size.height / 20), cc.p(size.width, size.height / 4+20), cc.color(255, 255, 255, 150), 2, cc.color(255, 255, 255, 50));

        var item1 = new cc.MenuItemImage(res.S_playNormal, res.S_playSelect, this.resetGame, this);
        item1.scaleX = 1.5;
        item1.setPosition(cc.p(0,-260));
        var menu = new cc.Menu(item1);
        this.addChild(menu, 11, 100);

        var action1 = cc.ScaleTo.create(0.5,1.7,1.2);
        var action2 = cc.ScaleTo.create(0.25,1.4,.9);
        var action3 = cc.ScaleTo.create(0.25,1.6,1.1);
        var sprite_sequence = cc.Sequence.create(action1,action2,action3);
        item1.runAction(cc.repeatForever(sprite_sequence));

        var label1 = new cc.LabelTTF("¡Vamos a Jugar!", "Arial", 38);
        // position the label on the center of the screen
        label1.x = size.width / 2;
        label1.y = size.height / 3 - 80;
        label1.color = cc.color(0,0,0);
        this.addChild(label1, 11);
    },
    resetGame: function(){
        can_play=true;
        cc.audioEngine.stopMusic();
        cc.audioEngine.stopAllEffects();
        var scene = new cc.Scene();
        scene.addChild(new PlayScene());
        cc.director.runScene(new cc.TransitionSlideInT(2, new PlayScene()));
    },
    initParticules:function(){

        this._emitter1 = new cc.ParticleSystem('res/Particle/LavaFlow.plist');
        this._emitter1.startColor = cc.color(255, 0, 0, 255);
        this._emitter2 = new cc.ParticleSystem('res/Particle/LavaFlow.plist');
        this._emitter2.startColor = cc.color(0, 255, 0, 255);
        this._emitter3 = new cc.ParticleSystem('res/Particle/LavaFlow.plist');
        this._emitter3.startColor = cc.color(0, 0, 255, 255);

        this._emitter1.stopSystem();
        this._emitter2.stopSystem();
        this._emitter3.stopSystem();

        var size = cc.winSize;
        this._emitter1.x = Math.floor((Math.random()*3*size.width/4)+size.width/4);
        this._emitter1.y = Math.floor((Math.random()*3*size.height/4)+size.height/4);
        this._emitter2.x = Math.floor((Math.random()*3*size.width/4)+size.width/4);
        this._emitter2.y = Math.floor((Math.random()*3*size.height/4)+size.height/4);
        this._emitter3.x = Math.floor((Math.random()*3*size.width/4)+size.width/4);
        this._emitter3.y = Math.floor((Math.random()*3*size.height/4)+size.height/4);
        this.addChild(this._emitter1,15);
        this.addChild(this._emitter2,15);
        this.addChild(this._emitter3,15);

        this.scheduleOnce( this.blueParticle, 0.2);
    },
    blueParticle:function(dt) {
        var size = cc.winSize;
        this._emitter1.totalParticles = 50;
        this._emitter1.x = Math.floor((Math.random()*3*size.width/4)+size.width/6);
        this._emitter1.y = Math.floor((Math.random()*3*size.height/4)+size.height/4);
        this._emitter2.stopSystem();
        this._emitter3.stopSystem();
        this.scheduleOnce( this.greeParticle, 0.7);
    },
    greeParticle:function(dt) {
        var size = cc.winSize;
        this._emitter2.totalParticles = 50;
        this._emitter2.x = Math.floor((Math.random()*3*size.width/4)+size.width/6);
        this._emitter2.y = Math.floor((Math.random()*3*size.height/4)+size.height/4);
        this._emitter1.stopSystem();
        this._emitter3.stopSystem();
        this.scheduleOnce( this.orangeParticle, 0.7);
    },
    orangeParticle:function(dt) {
        var size = cc.winSize;
        this._emitter3.totalParticles = 50;
        this._emitter3.x = Math.floor((Math.random()*3*size.width/4)+size.width/6);
        this._emitter3.y = Math.floor((Math.random()*3*size.height/4)+size.height/4);
        this._emitter2.stopSystem();
        this._emitter1.stopSystem();
        this.scheduleOnce( this.blueParticle, 0.7);
    }
});

var PlayScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new PlayLayer();
        this.addChild(layer);
    }
});

