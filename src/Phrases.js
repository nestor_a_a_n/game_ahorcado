/**
 * Created by nestor,andres.a@gmail.com on 4/04/15.
 */

/**
* Recuerde no usar tildes, el sistema no reconocerá las vocales que usen tilde.
*/
var Phrases = [
    "La posibilidad de realizar un sueño es lo que hace que la vida sea interesante",
    "Nunca desistas de un sueño. Solo trata de ver las señales que te lleven a el",
    "No que me hayas mentido, que ya no pueda creerte, eso me aterra",
    "La memoria es como el mal amigo; cuando mas falta te hace, te falla",
    "La verdad se corrompe tanto con la mentira como con el silencio",
    "Cuando alguien desea algo debe saber que corre riesgos y por eso la vida vale la pena",
    "Si has construido castillos en el aire, tu trabajo no se pierde; ahora coloca las bases debajo de ellos",
    "El que busca la verdad corre el riesgo de encontrarla",
    "No basta decir solamente la verdad, mas conviene mostrar la causa de la falsedad",
    "Como todos los soñadores, confundi el desencanto con la verdad",
    "Importa mucho mas lo que tu piensas de ti mismo que lo que los otros opinen de ti",
    "Que poco cuesta construir castillos en el aire y que cara es su destruccion",
    "La razon no me ha enseñado nada. Todo lo que yo se me ha sido dado por el corazon",
    "Me llamas tu vida, llamame tu alma; porque el alma es inmortal, y la vida es un dia",
    "Si es bueno vivir, todavia es mejor soñar, y lo mejor de todo, despertar",
    "La razon se compone de verdades que hay que decir y verdades que hay que callar",
    "La verdad triunfa por si misma, la mentira necesita siempre complicidad",
    "Una coleccion de pensamientos debe ser una farmacia donde se encuentra remedio a todos los males",
    "Solo es inmensamente rico aquel que sabe limitar sus deseos"
]