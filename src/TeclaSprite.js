/**
 * Created by nesto_000 on 3/04/15.
 */
var TeclaSprite = cc.Sprite.extend({
    _currentRotation:0,
    _scale:0.25,
    _n:null,
    _clickable:null,
    _key:null,
    _texture1:null,
    _texture2:null,
    ctor:function(x,y,n,key){
        this._texture1 = cc.textureCache.addImage("res/Sprite/letra_-"+n+".png");
        this._texture2 = cc.textureCache.addImage("res/Sprite/letra_roja_-"+n+".png");
        this._super(this._texture1);
        this.x = x;
        this.y = y;
        this.scale = this._scale;
        this.rotation=0;
        this.name= "key_"+n;
        this._n = n;
        this._key=key;
        this._clickable=true;
    },
    effectClickGood: function(){
        this._clickable=false;
        var sprite_action1 = cc.ScaleTo.create(0.2,-this._scale-0.2,this._scale+0.2);
        var sprite_action2 = cc.ScaleTo.create(0.2,this._scale+0.4,this._scale+0.4);
        var sprite_action21 = cc.DelayTime.create(0.2);
        var sprite_action3 = cc.ScaleTo.create(0.2,-this._scale,this._scale);
        var sprite_action4 = cc.ScaleTo.create(0.2,0,0);
        this.zIndex = 5;
        var soundClick = cc.callFunc(function (t) {
            cc.audioEngine.playEffect(res.Click_sound);
        }.bind(this));
        this.runAction(cc.sequence(soundClick,sprite_action1,sprite_action2,sprite_action21,sprite_action3,sprite_action4));
    },
    effectClickBad: function(){
        this._clickable=false;
        var sa1 = cc.ScaleTo.create(0.1,this._scale+0.05,this._scale+0.05);
        var sa2 = cc.ScaleTo.create(0.1,this._scale-0.05,this._scale-0.05);
        var sa3 = cc.ScaleTo.create(0.1,this._scale,this._scale);
        var sa4 = cc.RotateTo.create(0.1,36);
        var sa5 = cc.RotateTo.create(0.1,-36);
        var sa6 = cc.RotateTo.create(0.1,0);
        var soundWrong = cc.callFunc(function (t) {
            cc.audioEngine.playEffect(res.Wrong_sound);
        }.bind(this));
        var destroyMe = cc.callFunc(function (t) {
//            var spriteRed = new cc.Sprite("res/Sprite/letra_roja_-"+ t._n+".png");
//            spriteRed.x = t.x;
//            spriteRed.y = t.y;
//            spriteRed.scale = t.scale;
//            var that = t.getParent();
//            that.removeChild(t);
//            that.addChild(spriteRed);
            t.texture = t._texture2;
        }.bind(this));
        this.zIndex = 5;
        this.runAction(cc.sequence(destroyMe,soundWrong,sa1,sa2,sa1,sa2,sa1,sa2,sa1,sa3));
        this.runAction(cc.sequence(sa4,sa5,sa4,sa5,sa4,sa5,sa4,sa6));
    },
});