var folder = "res/Sprite/";

var res = {
    Backgound_play      : folder+"0_fondo.jpg",
    Banco_popular       : folder+"LOGOTIPO_BANCO_POPULAR.jpg",
    Prestaya            : folder+"maxresdefault.png",
    S_playNormal        : folder+"btn-play-normal.png",
    S_playSelect        : folder+"btn-play-selected.png",
    Star                : folder+"estrella.png",
    Wrong_1             : folder+"1_BASE.png",
    Wrong_2             : folder+"2_ESCALERA.png",
    Wrong_3             : folder+"3_POSTE.png",
    Wrong_4             : folder+"4_SOPORTE_POSTE.png",
    Wrong_5             : folder+"5_BRAZO_POSTE.png",
    Wrong_6             : folder+"6_LAZO.png",
    Wrong_7             : folder+"7_Ahorcado.png",
    Wrong_sound         : "res/Sound/wrong.mp3",
    Click_sound         : "res/Sound/click.mp3",
    Clock_sound         : "res/Sound/lock_quick_tick_tock.mp3",
    Clock_end_sound     : "res/Sound/chicharra.mp3",
    Lose_sound          : "res/Sound/lose.mp3",
    Win_sound           : "res/Sound/win.mp3",
    s_particles         : "res/Particle/particles.png",
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
g_resources.push("res/Particle/LavaFlow.plist");